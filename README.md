# README #
![Screenshot from 2017-06-25 23-50-41.png](https://bitbucket.org/repo/kMkLydM/images/3698007482-Screenshot%20from%202017-06-25%2023-50-41.png)
### What is this repository for? ###
A simple Gtk application for generating random passwords. Users can choose if they want to use UpperCase, Numbers, Special Character, as well as password length. The clear button clears the generated password as well as the password stored in the OS clipboard.

Version 1

### How do I get set up? ###
Development was done on a Fedora 25 machine using Python3.

Dependencies:

PyGobject

For Fedora use the install command from this link to install the PyGobject package.
On other linux distros, replacing 'dnf' with the package manager of your distro should install the proper packages.

https://developer.fedoraproject.org/tech/languages/python/pygobject.html

### Who do I talk to? ###

* Repo owner or admin