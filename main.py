import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
import random

class GridWindow(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self, title="Password Generator")

		self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
		grid = Gtk.Grid()
		self.add(grid)
		
		
		#defining widgets
		label1 = Gtk.Label(label='Password Length')
		label2 = Gtk.Label(label='Upper Case')
		label3 = Gtk.Label(label='Numbers')
		label4 = Gtk.Label(label='Special Characters')
		label5 = Gtk.Label(label='(Enter an integer for password length.)')
		label6 = Gtk.Label(label='(Include upper case letters?)')
		label7 = Gtk.Label(label='(Include Numbers?)')
		label8 = Gtk.Label(label='(Include special Characters?)')
		
		check2 = Gtk.CheckButton()
		check3 = Gtk.CheckButton()
		check4 = Gtk.CheckButton()
		
		self.lengthField = Gtk.Entry()
		self.passwordField = Gtk.Entry()
		
		copyButton = Gtk.Button(label='Copy')
		clearButton = Gtk.Button(label='Clear')
		generateButton = Gtk.Button(label='Generate')
		
		#laying out widgets
		grid.add(label1)
		grid.attach_next_to(self.lengthField, label1, Gtk.PositionType.RIGHT, 1, 1)
		grid.attach_next_to(label5, self.lengthField, Gtk.PositionType.RIGHT, 1, 1)
		
		grid.attach_next_to(label2, label1, Gtk.PositionType.BOTTOM, 1, 1)
		grid.attach_next_to(check2, label2, Gtk.PositionType.RIGHT, 1, 1)
		grid.attach_next_to(label6, check2, Gtk.PositionType.RIGHT, 1, 1)
		
		grid.attach_next_to(label3, label2, Gtk.PositionType.BOTTOM, 1, 1)
		grid.attach_next_to(check3, label3, Gtk.PositionType.RIGHT, 1, 1)
		grid.attach_next_to(label7, check3, Gtk.PositionType.RIGHT, 1, 1)
		
		grid.attach_next_to(label4, label3, Gtk.PositionType.BOTTOM, 1, 1)
		grid.attach_next_to(check4, label4, Gtk.PositionType.RIGHT, 1, 1)
		grid.attach_next_to(label8, check4, Gtk.PositionType.RIGHT, 1, 1)
		
		grid.attach_next_to(self.passwordField, label4, Gtk.PositionType.BOTTOM, 3, 1)
		grid.attach_next_to(copyButton, self.passwordField, Gtk.PositionType.RIGHT, 1, 1)
		grid.attach_next_to(clearButton, copyButton, Gtk.PositionType.RIGHT, 1, 1)
		grid.attach_next_to(generateButton, clearButton, Gtk.PositionType.RIGHT, 1, 1)
		
		#connecting widgets to methods
		check2.connect('toggled', self.upper_case_toggled, '0')
		check3.connect('toggled', self.numbers_toggled, '0')
		check4.connect('toggled', self.special_characters_toggled, '0')
		copyButton.connect('clicked', self.copy_clipboard)
		clearButton.connect('clicked', self.clear_password)
		generateButton.connect('clicked', self.generate_password)
		
		
#methods		
	def	upper_case_toggled(self, button, name):
		global characters
		if button.get_active():
			characters = characters + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
		else:
			characters = 'abcdefghijklmnopqrstuvwxyz'
		print(characters)	
		
	def numbers_toggled(self, button, name):
		global characters
		if button.get_active():
			characters = characters + '0123456789'
		else:
			characters = 'abcdefghijklmnopqrstuvwxyz'
		print(characters)	
		
	def special_characters_toggled(self, button, name):
		global characters
		if button.get_active():
			characters = characters + '[]{}!@#$%^&*()-+;:<>/?'
		else:
			characters = 'abcdefghijklmnopqrstuvwxyz'
		print(characters)
		
	def copy_clipboard(self, widget):
		self.clipboard.set_text(self.passwordField.get_text(), -1)
		return
		
	def clear_password(self, widget):
		text = ''
		self.passwordField.set_text(text)
		self.clipboard.set_text(self.passwordField.get_text(), -1)
		return
		
	def generate_password(self, widget):
		global characters
		length = self.lengthField.get_text()
		length = int(length)
		
		pw = str()
		for i in range(length):
			pw = pw + random.choice(characters)
		
		self.passwordField.set_text(pw)
		return
		
global characters
characters = 'abcdefghijklmnopqrstuvwxyz'
		
win = GridWindow()
win.connect('delete-event', Gtk.main_quit)
win.show_all()
Gtk.main()
